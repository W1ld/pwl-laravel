@extends('template.template')
@section('judul', 'Hobi saya')
@section('hobi', 'active')
@section('body')
<div class="container mt-1">

    <h2>Hobi saya</h2>
    <ol>
    @foreach($data as $hobi)
    <p>
        <!-- <li>{{ $hobi[0],$hobi[1] }}</li> -->
        <li>{{ $hobi }}</li>
    </p>
    @endforeach
    </ol>
</div>
@endsection