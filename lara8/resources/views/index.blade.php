@extends('template.template')
@section('judul', 'Nama Saya')
@section('nama', 'active')
@section('body')
<div class="container mt-1">

    <h2>Nama saya {{ $full }}</h2><br>
    <p>biasa dipanggil <b><i>{{ $call }}</i></b></p>

</div>
@endsection
