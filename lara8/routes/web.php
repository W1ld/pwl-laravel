<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $nama=[
        'call' => 'Wildan',
        'full' => 'Muhammad Wildan Cahya Purnama'
    ];
    return view('index', $nama);
});

Route::get('/hobi', function () {
    // $data =[
    //     ["Bermain",""], 
    //     ["Belajar",""], 
    //     ["Salto",""]];
    $data =["Bermain", "Belajar", "Salto"];
    return view('hobi', ['data'=>$data] );
});

Route::get('/kontak', function () {
    $data=[
        // 'wa' => "https://wa.me/085156748770?text=aku ingin kenalan ambek sampeyan"
        'wa' => "https://s.id/zaSGc"
    ];
    return view('kontak', $data );
});
